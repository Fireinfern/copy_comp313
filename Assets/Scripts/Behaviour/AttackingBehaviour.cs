using Mechanics;
using Unity.Netcode;
using UnityEngine;

public class AttackingBehaviour : StateMachineBehaviour
{
    private static readonly int Attacking = Animator.StringToHash("isAttacking");
    private float _timer;

    private CharacterNetwork _characterNetwork;

    private Collider2D _enemy;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!NetworkManager.Singleton.IsServer) return;
        _timer = 0;
        _characterNetwork = animator.GetComponent<CharacterNetwork>();
        if (_characterNetwork == null) return;
        _enemy = _characterNetwork.FindEnemy();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!NetworkManager.Singleton.IsServer) return;
        _timer += Time.deltaTime;
        if (_timer < 0.6) return;

        var enemyHealthComponent = _enemy == null ? null : _enemy.GetComponent<HealthComponent>();
        if (enemyHealthComponent == null || enemyHealthComponent.IsDead())
        {
            animator.SetBool(Attacking, false);
            _characterNetwork.SetIsStopped(false);
            return;
        }
        
        _characterNetwork.Attack(enemyHealthComponent, _enemy.tag);
        _timer = 0;
    }
}
