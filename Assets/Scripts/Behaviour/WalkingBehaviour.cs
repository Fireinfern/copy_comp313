using Unity.Netcode;
using UnityEngine;

public class WalkingBehaviour : StateMachineBehaviour
{
    private static readonly int Attacking = Animator.StringToHash("isAttacking");
    private static readonly int Idle = Animator.StringToHash("isIdle");

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!NetworkManager.Singleton.IsServer) return;
        var characterNetwork = animator.GetComponent<CharacterNetwork>();
        if (characterNetwork == null) return;
        if (!characterNetwork.HasEnemyInFront())
        {
            if (!characterNetwork.IsLastAttackedEnemyATower()) return;
            characterNetwork.SetIsStopped(true);
            animator.SetBool(Idle, true);
            return;
        }
        characterNetwork.SetIsStopped(true);
        animator.SetBool(Attacking, true);
    }
}
