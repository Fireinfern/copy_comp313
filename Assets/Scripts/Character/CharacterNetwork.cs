using System;
using Game;
using Game.Layers;
using Mechanics;
using Unity.Netcode;
using UnityEngine;

public class CharacterNetwork : NetworkBehaviour
{
    [SerializeField] 
    private float movementSpeed = 20.0f;

    private Transform _destination;
    
    private int _targetInteractableLayer;

    private int _direction;

    private bool _isStopped;

    private string _lastAttackedEnemyTag;

    /// <summary>
    /// Setup required fields for character mechanics
    /// </summary>
    /// <param name="destination">Set the destination of the character movement -> EnemyTower</param>
    /// <param name="playerLayers"></param>
    public void Setup(Transform destination, PlayerLayers playerLayers)
    {
        if (!IsServer) return; 
        if (destination == null)
        {
            throw new Exception("Destination cannot be null");
        }
        _destination = destination;
        var layer = playerLayers.OwnInteractableGameLayer.GetLayerIndex();
        gameObject.layer = layer;
        SetLayerClientRpc(layer);
        _targetInteractableLayer = playerLayers.TargetInteractableGameLayer.LayerMask;
        _direction = GetDirection();
        Flip();
    }

    [ClientRpc]
    private void SetLayerClientRpc(int layer)
    {
        gameObject.layer = layer;
    }

    public void SetIsStopped(bool isStopped)
    {
        _isStopped = isStopped;
    }

    private void Update()
    {
        if (!IsServer) return;
        if (_isStopped)
        {
            return;
        }
        transform.Translate(new Vector3(movementSpeed * _direction * Time.deltaTime, 0, 0));
    }

    public bool HasEnemyInFront()
    {
        if (!IsServer) return false;
        return FindEnemy() != null;
    }
    
    public void Attack(Damageble enemyDamageable, string enemyTag)
    {
        if (!IsServer) return;
        if (enemyDamageable == null) return;
        enemyDamageable.ApplyDamage(20, out _);
        _lastAttackedEnemyTag = enemyTag;
    }

    public Collider2D FindEnemy()
    {
        if (!IsServer) return null;
        return Physics2D.OverlapCircle(transform.position, 0.2f, _targetInteractableLayer);
    }

    public bool IsLastAttackedEnemyATower()
    {
        return _lastAttackedEnemyTag == GameTags.Instance.Tower;
    }

    private int GetDirection()
    {
        if (_destination == null)
        {
            throw new Exception("Destination cannot be null");
        }

        return _destination.position.x > transform.position.x ? 1 : -1;
    }

    /// <summary>
    /// This will flip the direction the character is looking to
    /// </summary>
    private void Flip()
    {
        var auxObject = gameObject;
        var currentScale = auxObject.transform.localScale;
        currentScale.x *= _direction;
        auxObject.transform.localScale = currentScale;
    }
}
