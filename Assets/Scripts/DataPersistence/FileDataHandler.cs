using System;
using System.IO;
using Newtonsoft.Json;
using Online;
using UnityEngine;

namespace DataPersistence
{
    public class FileDataHandler
    {
        private readonly string _dataDirPath = "";
        private readonly string _dataFileName = "";
        private readonly bool _useEncryption = false;
        private const string EncryptionCodeWord = "word";

        public FileDataHandler(string dataDirPath, string dataFileName, bool useEncryption) 
        {
            _dataDirPath = dataDirPath;
            _dataFileName = dataFileName;
            _useEncryption = useEncryption;
        }

        public LocalPlayerData Load() 
        {
            // use Path.Combine to account for different OS's having different path separators
            var fullPath = Path.Combine(_dataDirPath, _dataFileName);
            LocalPlayerData loadedData = null;
            if (!File.Exists(fullPath)) return loadedData;
            try 
            {
                // load the serialized data from the file
                var dataToLoad = "";
                using (var stream = new FileStream(fullPath, FileMode.Open))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        dataToLoad = reader.ReadToEnd();
                    }
                }

                // optionally decrypt the data
                if (_useEncryption) 
                {
                    dataToLoad = EncryptDecrypt(dataToLoad);
                }

                // deserialize the data from Json back into the C# object
                //loadedData = JsonUtility.FromJson<LocalPlayerData>(dataToLoad);
                loadedData = JsonConvert.DeserializeObject<LocalPlayerData>(dataToLoad);
            }
            catch (Exception e) 
            {
                Debug.LogError("Error occured when trying to load data from file: " + fullPath + "\n" + e);
            }
            //loadedData?.OnAfterDeserialize();
            return loadedData;
        }

        public void Save(LocalPlayerData data) 
        {
            //data.OnBeforeSerialize();
            // use Path.Combine to account for different OS's having different path separators
            var fullPath = Path.Combine(_dataDirPath, _dataFileName);
            try
            {
                var dirName = Path.GetDirectoryName(fullPath);

                if (dirName == null)
                {
                    throw new Exception("Dir cannot be null");
                }
                Debug.Log($"Saving at: {fullPath}");
                // create the directory the file will be written to if it doesn't already exist
                Directory.CreateDirectory(dirName);

                // serialize the C# game data object into Json
                //var dataToStore = JsonUtility.ToJson(data, true);
                var dataToStore = JsonConvert.SerializeObject(data);
                // optionally encrypt the data
                if (_useEncryption) 
                {
                    dataToStore = EncryptDecrypt(dataToStore);
                }

                // write the serialized data to the file
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    using (var writer = new StreamWriter(stream)) 
                    {
                        writer.Write(dataToStore);
                    }
                }
                Debug.Log("Data saved successfully");
            }
            catch (Exception e) 
            {
                Debug.LogError("Error occured when trying to save data to file: " + fullPath + "\n" + e);
            }
        }

        // the below is a simple implementation of XOR encryption
        private static string EncryptDecrypt(string data) 
        {
            var modifiedData = "";
            for (var i = 0; i < data.Length; i++) 
            {
                modifiedData += (char) (data[i] ^ EncryptionCodeWord[i % EncryptionCodeWord.Length]);
            }
            return modifiedData;
        }
    }
}