using Online;

namespace DataPersistence
{
    // implement this interface to handle Player local data on load/save
    public interface IPersistenceDataHandler
    {
        void OnLoad(LocalPlayerData data);
        // You can update reference data and it will be saved
        // This callback is called before save
        void OnBeforeSaveData(LocalPlayerData data);
    }
}