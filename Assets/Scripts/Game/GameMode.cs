﻿using System;
using System.Collections;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;

namespace Game
{
    public enum GameState
    {
        None = 0,
        PreGame = 1,
        InProgress = 2,
        PostGame = 3
    }
    
    public class GameMode : NetworkBehaviour
    {
        public static GameMode Instance { get; private set; }

        private NetworkVariable<GameState> gameModeState = new NetworkVariable<GameState>(GameState.None);
        
        public UnityEvent onGameStarted;

        [SerializeField] private int playersToStartGame = 2;

        [SerializeField] private NetworkObject player1TowerObject;

        [SerializeField] private NetworkObject player2TowerObject;
        
        public GameState GetGameState()
        {
            return gameModeState.Value;
        }
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                return;
            }

            if (Instance != this)
            {
                Destroy(this);
            }
        }

        private void Start()
        {
            NetworkManager.Singleton.OnServerStarted += OnServerInitialized;
        }

        private void OnServerInitialized()
        {
            gameModeState.Value = GameState.PreGame;
            NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
            Debug.Log("Bind to OnClient Connected Callback");
        }
    
        private void OnClientConnected(ulong clientId)
        {
            Debug.Log("Client Connected " + clientId);
            if (gameModeState.Value != GameState.PreGame) return;
            Debug.Log(NetworkManager.Singleton.ConnectedClients.Count);
            if (NetworkManager.Singleton.ConnectedClients.Count == 1)
            {
                Debug.Log("Assign Player " + clientId + " to Tower 1");
                player1TowerObject.ChangeOwnership(clientId);
            }
            else if (NetworkManager.Singleton.ConnectedClients.Count == 2)
            {
                player2TowerObject.ChangeOwnership(clientId);
            }
            if (NetworkManager.Singleton.ConnectedClients.Count >= playersToStartGame)
            {
                StartCoroutine(StartGameCorutine());
                return;
            }
        }

        private IEnumerator StartGameCorutine()
        {
            yield return new WaitForSeconds(3.0f);
            StartGame();
        }

        private void StartGame()
        {
            gameModeState.Value = GameState.InProgress;
            onGameStarted.Invoke();
            StartGameClientRpc();
        }

        [ClientRpc]
        private void StartGameClientRpc()
        {
            onGameStarted.Invoke();
        }

        [ServerRpc(RequireOwnership = false)]
        public void SurrenderServerRpc()
        {
            Debug.Log("Hello There");
        }

        public void EndGame()
        {
            gameModeState.Value = GameState.PostGame;
        }
    }
}