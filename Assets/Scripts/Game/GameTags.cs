using UnityEngine;

namespace Game
{
    public class GameTags : MonoBehaviour
    {
        [SerializeField] private string tower;

        public string Tower => tower;
    
        public static GameTags Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                return;
            }

            if (Instance != this)
            {
                Destroy(this);
            }
        }
    }
}