using UnityEngine;

namespace Game.Layers
{
    [CreateAssetMenu(fileName = "Layer/GameLayer", menuName = "Game Layer", order = 0)]
    public class GameLayer : ScriptableObject
    {
        [SerializeField] private LayerMask layerMask;

        [SerializeField] private string layerName;

        public LayerMask LayerMask => layerMask;
        
        public int GetLayerIndex()
        {
            return LayerMask.NameToLayer(layerName);
        }
    }
}