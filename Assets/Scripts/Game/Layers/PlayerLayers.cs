using UnityEngine;

namespace Game.Layers
{
    [CreateAssetMenu(fileName = "Layer/PlayerLayers", menuName = "Player Layers", order = 0)]
    public class PlayerLayers : ScriptableObject
    {
        [SerializeField] private GameLayer ownInteractableGameLayer;
        [SerializeField] private GameLayer targetInteractableGameLayer;

        public GameLayer OwnInteractableGameLayer => ownInteractableGameLayer;

        public GameLayer TargetInteractableGameLayer => targetInteractableGameLayer;
    }
}