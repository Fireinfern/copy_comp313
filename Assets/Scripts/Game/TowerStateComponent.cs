﻿using System;
using Mechanics;
using Online;
using Unity.Netcode;
using UnityEngine;

namespace Game
{
    public class TowerStateComponent : NetworkBehaviour
    {

        [SerializeField] private PlayerHudController playerHudController;

        [SerializeField] private Canvas winCanvas;

        [SerializeField] private Canvas loseCanvas;

        [SerializeField] private Spawner spawner;

        private void Start()
        {
            GetComponent<HealthComponent>()?.onHealthDepleated.AddListener(TowerDied);
        }

        public override void OnGainedOwnership()
        {
            if (IsServer) return;
            playerHudController.SetupData(LocalPlayerSubsystem.Instance.DeckOfUnits, spawner);
        }

        private void TowerDied()
        {
            if (IsServer) return;
            if (IsOwner)
            {
                loseCanvas.enabled = true;
                LocalPlayerSubsystem.Instance.LocalPlayerLose();
            }
            else
            {
                winCanvas.enabled = true;
                LocalPlayerSubsystem.Instance.LocalPlayerWon();
            }
        } 

        public override void OnLostOwnership()
        {
            base.OnLostOwnership();
            
        }
    }
}