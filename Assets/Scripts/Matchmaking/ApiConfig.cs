﻿using UnityEngine;

namespace Matchmaking
{
    [CreateAssetMenu(fileName = "ApiConfig", menuName = "ApiConfig", order = 0)]
    public class ApiConfig : ScriptableObject
    {
        [SerializeField]
        private string matchmakingUri = "http://202.81.237.99:5000/api/data";

        public string MatchmakingUri => matchmakingUri;
    }
}