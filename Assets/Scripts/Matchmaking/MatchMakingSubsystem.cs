﻿using System;
using System.Collections;
using Game;
using Online;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;


namespace Matchmaking
{
    [System.Serializable]
    public class MatchmakingServerResponse
    {
        public string apireturnip;
        public ushort apireturnport;
    }

    public struct MatchmakingServerData
    {
        public string ServerIp;
        public string ServerPort;
    }

    public class MatchMakingSubsystem : MonoBehaviour
    {
        public static MatchMakingSubsystem Instance { private set; get; }
        
        [SerializeField] public UnityEvent onFoundMatch = new UnityEvent();

        [SerializeField] public UnityEvent onStartToSearchMatch = new UnityEvent();

        // Config Scriptable Object
        [SerializeField] private ApiConfig apiConfig;

        // The Current Name of the Scene We are going to use to use network
        [SerializeField] private string matchSceneName;

        private Coroutine _findingMatchCoroutine;

        private bool _isInMatch = false;

        // Public Getter of IsInMatch
        public bool IsInMatch => _isInMatch;

        // Server Response Object to be use as a private setter
        private MatchmakingServerResponse _serverResponse = new MatchmakingServerResponse();

        // Public Getter of Server Response
        public MatchmakingServerResponse ServerResponse => _serverResponse;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
                SceneManager.sceneLoaded += OnSceneLoaded;
                return;
            }

            if (Instance != this)
            {
                Destroy(this);
            }
        }

        private void OnSceneLoaded(Scene loadedScene, LoadSceneMode loadSceneMode)
        {
            if (!loadedScene.name.Equals(matchSceneName))
                return;
            if (IsInMatch)
                return;
            _isInMatch = true;
            var networkManager = NetworkManager.Singleton;
            if (networkManager == null)
            {
                Debug.LogError("No network Manager in the scene");
                return;
            }

            var transport = networkManager.GetComponent<UnityTransport>();
            transport.ConnectionData.Address = _serverResponse.apireturnip;
            transport.ConnectionData.Port = _serverResponse.apireturnport;
            networkManager.OnClientStarted += () =>
            {
                GameMode.Instance.SurrenderServerRpc();
            };
            networkManager.StartClient();
        }

        public void StartSearchingMatch()
        {
            onStartToSearchMatch.Invoke();
            _findingMatchCoroutine = StartCoroutine(RequestMatch());
        }

        /// <summary>
        /// Request Match is a Corutine that will find a server to connect the player so he can start the game 
        /// </summary>
        private IEnumerator RequestMatch()
        {
            // Create a get Request from the server
            using (UnityWebRequest request = UnityWebRequest.Get(apiConfig.MatchmakingUri))
            {
#if UNITY_EDITOR
                // If in Editor Mode just wait 10 seconds and connect
                yield return new WaitForSeconds(10.0f);
#else
                // Send the request and wait
                yield return request.SendWebRequest();
                // If any errors print error and break Corutine
                if (
                    request.result == UnityWebRequest.Result.ConnectionError
                    || request.result == UnityWebRequest.Result.ProtocolError
                )
                {
                    Debug.LogError("Error: " + request.error);
                    yield break;
                }

                // Successfully received the response
                Debug.Log("Received: " + request.downloadHandler.text);
#endif
                
#if UNITY_EDITOR
                // Fill server response copy with local host
                _serverResponse.apireturnip = "127.0.0.1";
                _serverResponse.apireturnport = 7777;
#else
                // Deserialize Data from response and assign it to the server Response copy
                var deserializeData = JsonUtility.FromJson<MatchmakingServerResponse>(
                    request.downloadHandler.text
                );
                _serverResponse.apireturnip = deserializeData.apireturnip.ToString();
                _serverResponse.apireturnport = ushort.Parse(deserializeData.apireturnport.ToString());
#endif
                Debug.Log(_serverResponse.apireturnip);
                onFoundMatch.Invoke();
            }
        }

        public void CancelMatch()
        {
            StopCoroutine(_findingMatchCoroutine);
        }

        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
    }
}