﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Unity.Netcode;
using UnityEngine;

namespace Mechanics
{
    // Damaga Receiver is the class that will be near a trigger or a collision that can recieve damage
    public class DamageReceiver : NetworkBehaviour
    {

        [SerializeField] private float damageMultiplier = 1.0f;

        [ItemCanBeNull] private List<Damageble> _damagebles;

        private List<GameObject> _instigatorList = new List<GameObject>();

        private void Start()
        {
            _damagebles = new List<Damageble>(GetComponentsInParent<Damageble>());
            _damagebles.AddRange(GetComponents<Damageble>());

        }

        // Reset Instigators on Late Update
        private void LateUpdate()
        {
            _instigatorList.RemoveAll((instigator) => (true));
        }

        
        // Send Damage to the Damagable Compoments
        public void ApplyDamage(float damageAmount, GameObject instigator)
        {
            if (!IsServer) return;
            if (_instigatorList.Contains(instigator)) return;
            _instigatorList.Add(instigator);
            // Order all damagable components according to their priority 
            var damagablesInOrder = _damagebles.OrderByDescending(damageble => damageble.GetDamagePriority());
            float remainingDamage = damageAmount;
            // Set the damage until all damage has been distributed
            foreach (var damagable in damagablesInOrder)
            {
                damagable.ApplyDamage(remainingDamage * damageMultiplier, out remainingDamage);
                if (remainingDamage <= 0.0) break;
            }
        }
        
    }
}