﻿namespace Mechanics
{
    // Interface to be use in any component that can receive damage
    public interface Damageble
    {
        // Apply damage, this must be done just in server if possible
        void ApplyDamage(float value, out float rest);

        // Override this function to give a highert or lower priority to this component in case you have more
        int GetDamagePriority();
    }
}