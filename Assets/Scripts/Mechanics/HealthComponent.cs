﻿using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;

namespace Mechanics
{
    // Basic health component that will go on the character
    // This will replicate
    public class HealthComponent : NetworkBehaviour, Damageble
    {
        // Value to replicate
        private NetworkVariable<float> _health = new NetworkVariable<float>();
        
        // Initial Value of Health
        [SerializeField]
        private const float HealthInitialValue = 100.0f;

        // event triggered when health chages
        [SerializeField] public UnityEvent<float, float> onHealthChangeEvent;

        // event triggered when there is no health
        [SerializeField] public UnityEvent onHealthDepleated;

        // start the values when it spawns on network
        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();
            if (IsServer)
            {
                _health.Value = HealthInitialValue;
            }

            _health.OnValueChanged += OnHealthChanged;
        }
        
        public override void OnNetworkDespawn()
        {
            base.OnNetworkDespawn();
            _health.OnValueChanged -= OnHealthChanged;
        }
        
        // Callback when helth chances on client
        private void OnHealthChanged(float previous, float current)
        {
            onHealthChangeEvent.Invoke(previous, current);
            if (current <= 0.0f)
            {
                onHealthDepleated.Invoke();
            }
        }

        // Pass to a Damagable Interface
        [ServerRpc(RequireOwnership = false, Delivery = RpcDelivery.Reliable)]
        public void ReduceHealthServerRpc(float value)
        {
            _health.Value = Mathf.Max(0.0f, _health.Value - value);
        }

        // override of the interface
        public void ApplyDamage(float value, out float rest)
        {
            if (!IsServer)
            {
                rest = value;
                return;
            }
            var resultingHealth = _health.Value - value;
            _health.Value = Mathf.Max(0.0f, resultingHealth);
            rest = 0.0f;
            if (resultingHealth < 0.0f)
            {
                rest -= resultingHealth;
            }

            if (resultingHealth <= 0.0f)
            {
                onHealthDepleated.Invoke();
            }
        }

        // Verify if is dead
        public bool IsDead()
        {
            return _health.Value <= 0;
        }

        // Obtain the priority on the list of Damagables
        public int GetDamagePriority()
        {
            return 1;
        }
    }
}