using Unity.Netcode;
using UnityEngine;

/// <summary>
/// This class is meant to Despawn the NetworkObject (+ Destroy game object) when the life gets to 0.
/// </summary>
public class HeathZeroDestroyer : MonoBehaviour
{
    public void OnHealthChanged(float previous, float current)
    {
        if (current <= 0)
        {
            
            // Destroy(gameObject);
        }
    }

    public void OnHealthDepleted()
    {
        if (!NetworkManager.Singleton.IsServer) return;
        // GetComponent<NetworkObject>()?.Despawn();
        // Destroy(gameObject);
    }
}
