﻿using System;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

namespace Mechanics
{
    // Client only for canvas controller
    public class PlayerHudController : MonoBehaviour
    {
        // Units for spawning, this will change in when we recive this units from the player
        [SerializeField] private List<ScriptableObjectUnits> unitsForSpawning = new List<ScriptableObjectUnits>();

        // references to the buttons
        [SerializeField] private List<UnitButtonTrigger> unitButtonTriggers = new List<UnitButtonTrigger>();

        // Reference to the spawner that will spawn the units
        [SerializeField] private Spawner spawner;

        private void SetupData()
        {
            for (int i = 0; i < unitsForSpawning.Count; i++)
            {
                unitButtonTriggers[i].SetData(unitsForSpawning[i]);
                unitButtonTriggers[i].OnButtonClick.AddListener(OnSpawnRequested);
            }

        }

        public void SetupData(List<ScriptableObjectUnits> deck, Spawner selectedSpawner)
        {
            for (int i = 0; i < unitsForSpawning.Count; i++)
            {
                unitButtonTriggers[i].OnButtonClick.RemoveListener(OnSpawnRequested);
            }
            unitsForSpawning = deck;
            spawner = selectedSpawner;
            SetupData();
        }

        private void OnSpawnRequested(ScriptableObjectUnits objectUnit)
        {
            spawner.SpawnSolider(objectUnit.objectInternalId);
        }
    }
}