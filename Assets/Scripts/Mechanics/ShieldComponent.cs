﻿using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;

namespace Mechanics
{
    public class ShieldComponent : NetworkBehaviour, Damageble
    {
        private NetworkVariable<float> shield = new NetworkVariable<float>();

        [SerializeField] private const float shieldInitialValue = 100.0f;

        public UnityEvent<float, float> onShieldChangedEvent;

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();
            if (IsServer)
            {
                shield.Value = shieldInitialValue;
            }

            shield.OnValueChanged += OnShieldChanged;
        }

        public override void OnNetworkDespawn()
        {
            base.OnNetworkDespawn();
            shield.OnValueChanged -= OnShieldChanged;
        }

        private void OnShieldChanged(float previous, float current)
        {
            onShieldChangedEvent.Invoke(previous, current);
        }

        public void ApplyDamage(float value, out float rest)
        {
            rest = 0.0f;
            var resultingShield = shield.Value - value;
            shield.Value = Mathf.Max(shield.Value, 0.0f);
            if (resultingShield < 0.0f)
            {
                rest -= resultingShield;
            }
        }

        public int GetDamagePriority()
        {
            return 2;
        }
    }
}