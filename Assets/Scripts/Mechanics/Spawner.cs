using System;
using System.Collections;
using System.Collections.Generic;
using Game.Layers;
using units;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Spawner : NetworkBehaviour
{
    //private GameObject soliderToSpawn;
    [SerializeField] private Transform enemyTowerTransform;
    //[SerializeField] private int maxObjectInstanceCount = 3;

    [SerializeField] private UnitDictionary unitDictionary;

    [SerializeField] private PlayerLayers playerLayers;
    
    public void SpawnSolider(uint unitId)
    {
        SpawnSoldierServerRpc(unitId);
    }

    [ServerRpc(RequireOwnership = false)]
    private void SpawnSoldierServerRpc(uint unitId)
    {
        if (!IsServer) return;
        var unit = unitDictionary.GetUnitById(unitId);
        if (unit == null)
        {
            Debug.LogError("No such unit in the dictionary");
            return;
        }

        if (enemyTowerTransform == null)
        {
            Debug.Log("No enemy tower was set or might be destroyed");
            return;
        }

        var soldierToSpawn = unit.rig;
        var soliderInstance = Instantiate(soldierToSpawn, transform.position, transform.rotation);
        var soliderNetworkIbject = soliderInstance.GetComponent<NetworkObject>();
        var characterMovement = soliderInstance.GetComponent<CharacterNetwork>();
        soliderNetworkIbject.Spawn(true);
        characterMovement.Setup(enemyTowerTransform, playerLayers);
    }
}
