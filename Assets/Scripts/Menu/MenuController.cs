﻿using System;
using Matchmaking;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class MenuController : MonoBehaviour
    {
        [SerializeField] private GameObject findingGameModal;

        public void FindGame()
        {
            var matchmakingInstance = MatchMakingSubsystem.Instance;
            if (matchmakingInstance == null) return;
            findingGameModal.SetActive(true);
            matchmakingInstance.onFoundMatch.AddListener(CloseFindMatchModal);
            matchmakingInstance.StartSearchingMatch();
        }

        public void GoToProfile()
        {
            SceneManager.LoadScene("player_profile");
        }

        void CloseFindMatchModal()
        {
            findingGameModal.SetActive(false);
            var matchmakingInstance = MatchMakingSubsystem.Instance;
            if (matchmakingInstance == null) return;
            matchmakingInstance.onFoundMatch.RemoveListener(CloseFindMatchModal);
            SceneManager.LoadScene("battle_ui");
        }

        private void OnDestroy()
        {
            var matchmakingInstance = MatchMakingSubsystem.Instance;
            if (matchmakingInstance == null) return;
            matchmakingInstance.onFoundMatch.RemoveListener(CloseFindMatchModal);
        }
    }
}
