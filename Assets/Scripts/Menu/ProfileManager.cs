using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using DataPersistence;
using Online;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProfileManager : MonoBehaviour, IPersistenceDataHandler
{
    [SerializeField]
    private TMP_InputField playerNameInputField;

    [SerializeField]
    private TMP_InputField scoreInputField;

    private void Start()
    {
        SetData(LocalPlayerSubsystem.Instance.PlayerData);
    }

    public void OnLoad(LocalPlayerData data)
    {
        SetData(data);
    }

    private void SetData(LocalPlayerData data)
    {
        if (data == null) return;
        Debug.Assert(playerNameInputField != null);
        playerNameInputField.text = data.PlayerName;
        Debug.Assert(scoreInputField != null);
        scoreInputField.text = data.Score.ToString();
    }

    public void OnBeforeSaveData(LocalPlayerData data)
    {
        data.PlayerName = playerNameInputField.text;
    }

    public void GoBackToMenu()
    {
        SceneManager.LoadScene("main_menu");
    }

    public void SaveNewName()
    {
        LocalPlayerSubsystem.Instance.SaveGame();
    }
}
