﻿using System;
using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Online
{
    public enum RequestType
    {
        GET = 0,
        POST = 1,
        PUT = 2
    }
    
    public class HttpConnection : MonoBehaviour
    {
        public static HttpConnection Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
                return;
            }

            if (Instance != this)
            {
                Destroy(this);
            }
        }

        public UnityWebRequest CreateRequest(string path, RequestType type = RequestType.GET, object data = null)
        {
            var request = new UnityWebRequest(path, type.ToString());

            if (data != null)
            {
                var bodyRaw = Encoding.UTF8.GetBytes(JsonUtility.ToJson(data));
                request.uploadHandler = new UploadHandlerRaw(bodyRaw);
            }

            request.downloadHandler = new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-type", "application/json");

            return request;
        }

        public void AttachHeader(ref UnityWebRequest request, string key, string value)
        {
            request.SetRequestHeader(key, value);
        }
    }
}