﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Numerics;
using DataPersistence;
using units;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.Serialization;

namespace Online
{
    [Serializable]
    public class LocalPlayerData
    {
        public String PlayerName = "DefaultName2";

        public BigInteger Score = 1000;

        public List<uint> DeckIds;
    }

    [Serializable]
    public class PlayerResponse
    {
        public String name;

        public BigInteger elo_score;
    }

    [Serializable]
    public class PlayerRequestData
    {
        public PlayerRequestData(string newName, string newPassword)
        {
            name = newName;
            password = newPassword;
        }

        public String name;

        public String password;
    }

    [Serializable]
    public class GameResultForPlayer
    {

        public GameResultForPlayer(string newName, string newPassword, string newOpponentScore)
        {
            name = newName;
            password = newPassword;
            opponent_score = newOpponentScore;
        }
        
        public string name;

        public string password;

        public string opponent_score;
    }

    [Serializable]
    public class EloChangeResult
    {
        public string name;

        public BigInteger new_elo_score;
    }

    public class LocalPlayerSubsystem : MonoBehaviour
    {
        [Header("File Storage Config")] [SerializeField]
        private string fileName;

        [SerializeField] private bool useEncryption;

        public static LocalPlayerSubsystem Instance { get; private set; }

        [SerializeField] private LocalPlayerData _playerData;

        [SerializeField] private UnityEvent onSavedSuccessfully;

        public LocalPlayerData PlayerData => _playerData;

        [SerializeField] private List<ScriptableObjectUnits> _deckOfUnits;

        public List<ScriptableObjectUnits> DeckOfUnits => _deckOfUnits;

        [SerializeField] private UnitDictionary unitDictionary;

        private List<IPersistenceDataHandler> _persistenceDataHandlers;
        private FileDataHandler _dataHandler;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
                return;
            }

            if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            _dataHandler = new FileDataHandler(Application.persistentDataPath, fileName, useEncryption);
            _persistenceDataHandlers = FindAllDataPersistenceObjects();
            LoadGame();
        }

        private void NewGame()
        {
            _playerData = new LocalPlayerData();
        }

        private IEnumerator RequestPlayerData()
        {
            var requestData = new PlayerRequestData(_playerData.PlayerName, _playerData.PlayerName);
            using (UnityWebRequest request = UnityWebRequest.Post("http://202.81.237.99:5001/api/player",
                       JsonUtility.ToJson(requestData), "application/json"))
            {
                // Send the request and wait
                yield return request.SendWebRequest();
                // If any errors print error and break Corutine
                if (
                    request.result == UnityWebRequest.Result.ConnectionError
                    || request.result == UnityWebRequest.Result.ProtocolError
                )
                {
                    Debug.LogError("Error: " + request.error);
                    yield break;
                }

                var deserializedData = JsonUtility.FromJson<PlayerResponse>(request.downloadHandler.text);
                _playerData.PlayerName = deserializedData.name;
                _playerData.Score = deserializedData.elo_score;

                // push the loaded data to all other scripts that need it
                foreach (var dataPersistenceObj in _persistenceDataHandlers)
                {
                    dataPersistenceObj.OnLoad(_playerData);
                }
            }
        }

        private void LoadGame()
        {
            // load any saved data from a file using the data handler
            _playerData = _dataHandler.Load();

            if (_playerData.DeckIds != null && _playerData.DeckIds.Count == 5)
            {
                
                _deckOfUnits.Clear();
                foreach (var id in _playerData.DeckIds)
                {
                    _deckOfUnits.Add(unitDictionary.GetUnitById(id));
                }
            
            }
            else
            {
                _playerData.DeckIds = new List<uint>();
                foreach (var unit in _deckOfUnits)
                {
                    _playerData.DeckIds.Add(unit.objectInternalId);
                }
            }

            // if no data can be loaded, initialize to a new game
            if (_playerData == null)
            {
                Debug.Log("No data was found. Initializing data to defaults.");
                NewGame();
            }

            StartCoroutine(RequestPlayerData());

            // push the loaded data to all other scripts that need it
            foreach (var dataPersistenceObj in _persistenceDataHandlers)
            {
                dataPersistenceObj.OnLoad(_playerData);
            }
        }

        public void SaveGame()
        {
            // pass the data to other scripts so they can update it
            foreach (var dataPersistenceObj in _persistenceDataHandlers)
            {
                dataPersistenceObj.OnBeforeSaveData(_playerData);
            }

            // save that data to a file using the data handler
            _dataHandler.Save(_playerData);
            StartCoroutine(RequestPlayerData());
            onSavedSuccessfully?.Invoke();
        }

        public void UpdateDeckInPlayerData()
        {
            PlayerData.DeckIds.Clear();
            foreach (var unit in _deckOfUnits)
            {
                PlayerData.DeckIds.Add(unit.objectInternalId);
            }
        }

        private void OnApplicationQuit()
        {
            SaveGame();
        }

        private static List<IPersistenceDataHandler> FindAllDataPersistenceObjects()
        {
            var dataPersistenceObjects = FindObjectsOfType<MonoBehaviour>()
                .OfType<IPersistenceDataHandler>();

            return new List<IPersistenceDataHandler>(dataPersistenceObjects);
        }

        public void LocalPlayerWon()
        {
            StartCoroutine(PlayerWon());
        }

        // I know this could have been one functiuon only
        private IEnumerator PlayerWon()
        {
            var requestData = new GameResultForPlayer(_playerData.PlayerName, _playerData.PlayerName, _playerData.Score.ToString());
            using (UnityWebRequest request = UnityWebRequest.Post("http://202.81.237.99:5001/api/win",
                       JsonUtility.ToJson(requestData), "application/json"))
            {
                yield return request.SendWebRequest();
                if (
                    request.result == UnityWebRequest.Result.ConnectionError
                    || request.result == UnityWebRequest.Result.ProtocolError
                )
                {
                    Debug.LogError("Error: " + request.error);
                    yield break;
                }
                var deserializedData = JsonUtility.FromJson<EloChangeResult>(request.downloadHandler.text);
                _playerData.Score = deserializedData.new_elo_score;
            }
        }

        public void LocalPlayerLose()
        {
            StartCoroutine(PlayerLose());
        }

        private IEnumerator PlayerLose()
        {
            var requestData = new GameResultForPlayer(_playerData.PlayerName, _playerData.PlayerName, _playerData.Score.ToString());
            using (UnityWebRequest request = UnityWebRequest.Post("http://202.81.237.99:5001/api/loss",
                       JsonUtility.ToJson(requestData), "application/json"))
            {
                yield return request.SendWebRequest();
                if (
                    request.result == UnityWebRequest.Result.ConnectionError
                    || request.result == UnityWebRequest.Result.ProtocolError
                )
                {
                    Debug.LogError("Error: " + request.error);
                    yield break;
                }
                var deserializedData = JsonUtility.FromJson<EloChangeResult>(request.downloadHandler.text);
                _playerData.Score = deserializedData.new_elo_score;
            }
        }
    }
}