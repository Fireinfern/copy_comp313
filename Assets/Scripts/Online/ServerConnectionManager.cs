﻿using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using UnityEngine;
using UnityEngine.Rendering;

namespace Online
{
    public class ServerConnectionManager : MonoBehaviour
    {

        [SerializeField] private int maxAmountOfPlayers = 2;

        private int _currentAmountOfPlayers = 0;
        
        // _ip variable with default value of local host
        private string _ip = "127.0.0.1";

        private ushort _port = 7777;

        private bool _forceStartAsServer = false;
        
        // Receives the IP and the PORT for this server
        private void Start()
        {
            var args = System.Environment.GetCommandLineArgs();
            for (int i = 0; i < args.Length; i++)
            {
                // Get the Ip Arg
                if (args[i] == "-ip" && i + 1 < args.Length)
                {
                    _ip = args[i + 1].ToLower();
                    continue;
                }
                // Get the Port Arg
                if (args[i] == "-port" && i + 1 < args.Length)
                {
                    if (ushort.TryParse(args[i + 1], out ushort parsedPort))
                    {
                        _port = parsedPort;
                    }
                    else
                    {
                        Debug.LogError("Received a Wrong Port Number!");
                    }
                    continue;
                }
                // Parse server arg
                if (args[i] == "-server")
                {
                    _forceStartAsServer = true;
                    continue;
                }
            }
            // If the Game is running headless or you want to force a server
            if (SystemInfo.graphicsDeviceType != GraphicsDeviceType.Null || _forceStartAsServer)
            {
                gameObject.SetActive(false);
                return;
            }
            // Set transport data
            var transport = NetworkManager.Singleton.GetComponent<UnityTransport>();
            transport.ConnectionData.Address = _ip;
            transport.ConnectionData.Port = _port;
            // Start Server
            NetworkManager.Singleton.StartServer();
            NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
        }

        // Callback when a player connects to the game
        private void OnClientConnected(ulong clientId)
        {
            Debug.Log("Client Connected with the id = " + clientId);
            if (_currentAmountOfPlayers >= maxAmountOfPlayers)
            {
                NetworkManager.Singleton.DisconnectClient(clientId);
                return;
            }
            _currentAmountOfPlayers += _currentAmountOfPlayers;
        }

        private void OnClientDisconnected(ulong clientId)
        {
            
        }
    }
}