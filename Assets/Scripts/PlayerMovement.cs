using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PlayerMovement : NetworkBehaviour
{
    public float moveSpeed = 5f; // Ensure you have a default value or set this in the inspector
    public float jumpForce = 10f; // Ensure you have a default value or set this in the inspector
    
    private Rigidbody2D rb;
    private SpriteRenderer spriteRenderer;

    // NetworkVariable to synchronize facing direction
    private NetworkVariable<bool> isFacingRight = new NetworkVariable<bool>(true);

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (IsOwner)
        {
            float moveHorizontal = Input.GetAxis("Horizontal");

            // Send movement command to the server
            SubmitMovementRequestServerRpc(moveHorizontal * moveSpeed, Mathf.Abs(moveHorizontal) > 0.01f ? moveHorizontal > 0 : isFacingRight.Value);

            // Handle Jumping, send jump request to the server
            if (Input.GetButtonDown("Jump"))
            {
                SubmitJumpRequestServerRpc();
            }
        }

        // Apply the facing direction locally for all clients
        spriteRenderer.flipX = !isFacingRight.Value;
    }

    [ServerRpc]
    void SubmitMovementRequestServerRpc(float move, bool facingRight)
    {
        // Apply movement on the server
        rb.velocity = new Vector2(move, rb.velocity.y);

        // Update the facing direction if changed
        if (isFacingRight.Value != facingRight)
        {
            isFacingRight.Value = facingRight;
        }
    }

    [ServerRpc]
    void SubmitJumpRequestServerRpc(ServerRpcParams rpcParams = default)
    {
        // Check for ground contact before applying jump force to prevent multi-jumping
        if (Mathf.Abs(rb.velocity.y) < 0.001f)
        {
            rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
        }
    }
}
