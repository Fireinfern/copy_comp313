﻿using System;
using System.Collections.Generic;
using Online;
using UnityEngine;


public class DeckDataController : MonoBehaviour
{
    [SerializeField] private List<Deck_holder> deckHolders;

    private void Start()
    {
        LoadDataFromLocalPlayer();
    }

    private void LoadDataFromLocalPlayer()
    {
        var deck = LocalPlayerSubsystem.Instance.DeckOfUnits;
        var count = Math.Min(deck.Count, deckHolders.Count);
        for (var i = 0; i < count; i++)
        {
            deckHolders[i].UpdateData(deck[i]);
        }
    }

    public void SaveDataToLocalPlayer()
    {
        LocalPlayerSubsystem.Instance.DeckOfUnits.Clear();
        foreach (var t in deckHolders)
        {
            LocalPlayerSubsystem.Instance.DeckOfUnits.Add(t.Data);
        }

        LocalPlayerSubsystem.Instance.UpdateDeckInPlayerData();
        LocalPlayerSubsystem.Instance.SaveGame();
    }
}