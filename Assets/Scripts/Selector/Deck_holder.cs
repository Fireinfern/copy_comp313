using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Deck_holder : MonoBehaviour
{
    private ScriptableObjectUnits _recievedData;

    public ScriptableObjectUnits Data => _recievedData;

    private Image _unitImage;

    private void Start()
    {
        _unitImage = GetComponentInChildren<Image>();
    }

    public void UpdateData(ScriptableObjectUnits newData)
    {
        _recievedData = newData;
        if (_unitImage == null) return;
        _unitImage.sprite = _recievedData.largeSprite;
    }
}
