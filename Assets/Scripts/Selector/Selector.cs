using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Selector : MonoBehaviour
{
    public GameObject modalWindow;
    public ScriptableObjectUnits[] _reveivedData;
    public Deck_holder[] selector;

    public void clicker()
    {
        for (int i = 0; i < _reveivedData.Length; i++)
        {
            if (_reveivedData[i] != null)
            {
                if (_reveivedData[i].unitName == modalWindow.GetComponentInChildren<Text>().text)
                {
                    if (i % 2 == 0)
                    {
                        selector[i / 2].UpdateData(_reveivedData[i]);
                    }
                    else
                    {
                        selector[(i - 1) / 2].UpdateData(_reveivedData[i]);
                    }

                    Debug.Log("Button click!");
                    modalWindow.SetActive(false);
                    return;
                }
            }
        }
    }
}