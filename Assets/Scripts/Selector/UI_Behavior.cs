using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class UI_Behavior : MonoBehaviour
{
    public GameObject modalWindow;
    public ScriptableObjectUnits _reveivedData;
    public Button selecion;
    public Button selector;
    [SerializeField] private Image buttonImage;
    public UnityEvent<ScriptableObjectUnits> OnButtonClick;
    


    void Start()
    {
        //selecion.onClick.AddListener(mute);
        //selector.onClick.AddListener(clicker);
    }

    private void mute()
    {
        Debug.Log("Button bap!");
    }

    public void SetData(ScriptableObjectUnits newData)
    {
        _reveivedData = newData;
        buttonImage.sprite = _reveivedData.smallSprite;
        modalWindow.GetComponentInChildren<Text>().text = _reveivedData.unitName;
        modalWindow.GetComponentInChildren<HorizontalLayoutGroup>().GetComponentInChildren<Text>().text = _reveivedData.energyCost.ToString();
    }
    // Update is called once per frame
   
    
    public void clicked()
    {
        //Debug.Log("set up ");
        
        modalWindow.SetActive(true);
        modalWindow.GetComponentInChildren<Text>().text= _reveivedData.unitName;
        modalWindow.GetComponentsInChildren<Image>()[1].GetComponentInChildren<Image>().sprite = _reveivedData.largeSprite;
        //modalWindow.GetComponentInChildren<Image>().GetComponent<Animator>(). = _reveivedData.rig.GetComponent<Animator>().GetComponent<AnimationClip>();
        modalWindow.GetComponentsInChildren<HorizontalLayoutGroup>()[0].GetComponentInChildren<Text>().text ="Cost: "+ _reveivedData.energyCost.ToString();
        modalWindow.GetComponentsInChildren<HorizontalLayoutGroup>()[0].GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Attack: " + _reveivedData.damage.ToString();
        modalWindow.GetComponentsInChildren<HorizontalLayoutGroup>()[1].GetComponentsInChildren<TMPro.TextMeshProUGUI>(Equals("HP:"))[0].text = "HP: " + _reveivedData.healt.ToString();
        modalWindow.GetComponentsInChildren<HorizontalLayoutGroup>()[1].GetComponentsInChildren<TMPro.TextMeshProUGUI>()[1].text = "Shield: " + _reveivedData.shield.ToString();
    }
   
}
