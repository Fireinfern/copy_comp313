using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class EnemyAutomaticSpawner : MonoBehaviour
{
    [SerializeField] private ScriptableObjectUnits unitForSpawning;
    
    [SerializeField] private Spawner spawner;
    
    [SerializeField] private int spawnTimeInSecs;
    
    private float _timer;

    private void Start()
    {
        _timer = spawnTimeInSecs + 1;
    }

    private void Update()
    {
        if (!NetworkManager.Singleton.IsClient)
        {
            return;
        }
        _timer += Time.deltaTime;
        if (_timer < spawnTimeInSecs) return;
        spawner.SpawnSolider(unitForSpawning.objectInternalId);
        _timer = 0;
    }
}
