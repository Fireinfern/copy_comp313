﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Button))]
    public class GoToSceneButton : MonoBehaviour
    {

        [SerializeField] private string mapNameToTeleport = "main_menu";
        
        private void Start()
        {
            GetComponent<Button>().onClick.AddListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            SceneManager.LoadScene(mapNameToTeleport);
        }
    }
}