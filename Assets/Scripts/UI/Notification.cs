using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Notification : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI notificationText;

    [SerializeField] private int showMessageTime;
    
    private void Start()
    {
        Debug.Assert(notificationText != null);
    }

    public void ShowMessage(string message)
    {
        gameObject.SetActive(true);
        StartCoroutine(ShowMessageCoroutine(message, showMessageTime));
    }

    private IEnumerator ShowMessageCoroutine(string message, int time)
    {
        notificationText.text = message;
        yield return new WaitForSeconds(time);
        notificationText.text = "";
        gameObject.SetActive(false);
    }
}
