﻿using System;
using Online;
using TMPro;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;

namespace UI
{
    public class PlayerName : NetworkBehaviour
    {
        [SerializeField] private TMP_Text nameText;

        private NetworkVariable<FixedString64Bytes> playerName = new NetworkVariable<FixedString64Bytes>("");

        private void Start()
        {
            playerName.OnValueChanged += OnNameChanged;
        }

        public override void OnGainedOwnership()
        {
            base.OnGainedOwnership();
            SendPlayerNameServerRpc(LocalPlayerSubsystem.Instance.PlayerData.PlayerName);
        }

        [ServerRpc]
        private void SendPlayerNameServerRpc(string newPlayerName)
        {
            playerName.Value = newPlayerName;
        }

        private void OnNameChanged(FixedString64Bytes prevValue, FixedString64Bytes newValue)
        {
            nameText.SetText(newValue.ToString());
        }
        
    }
}