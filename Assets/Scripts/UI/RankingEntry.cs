﻿using TMPro;
using UnityEngine;

namespace UI
{
    public class RankingEntry : MonoBehaviour
    {
        [SerializeField] private TMP_Text playerName;

        [SerializeField] private TMP_Text score;

        public void UpdateTexts(string newName, string newScore)
        {
            playerName.SetText(newName);
            score.SetText(newScore);
        }
    }
}