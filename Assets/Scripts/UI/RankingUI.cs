﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace UI
{

    [Serializable]
    public class RankingPlayerData
    {
        public ulong elo_score;

        public string name;
    }

    [Serializable]
    public class RankingData
    {
        public List<RankingPlayerData> playerDatas;
    }
    
    public class JsonHelper
    {
        public static T[] getJsonArray<T>(string json)
        {
            string newJson = "{ \"array\": " + json + "}";
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>> (newJson);
            return wrapper.array;
        }
 
        [Serializable]
        private class Wrapper<T>
        {
            public T[] array;
        }
    }

    public class RankingUI : MonoBehaviour
    {

        [SerializeField] private GameObject layout;

        [SerializeField] private RankingEntry entryPrefab;
        
        private void Start()
        {
            StartCoroutine(RequestRanking());
        }

        private IEnumerator RequestRanking()
        {
            using (UnityWebRequest request = UnityWebRequest.Get("http://202.81.237.99:5001/api/top10"))
            {
                yield return request.SendWebRequest();
                if (
                    request.result == UnityWebRequest.Result.ConnectionError
                    || request.result == UnityWebRequest.Result.ProtocolError
                )
                {
                    Debug.LogError("Error: " + request.error);
                    yield break;
                }

                var deserializedData = JsonHelper.getJsonArray<RankingPlayerData>(request.downloadHandler.text);
                foreach (var playerData in deserializedData)
                {
                    entryPrefab.UpdateTexts(playerData.name, playerData.elo_score.ToString());
                    Instantiate(entryPrefab.gameObject, layout.transform);
                }
            }
        }

        private void BackToMainMenu()
        {
            SceneManager.LoadScene("main_menu");
        }
    }
}