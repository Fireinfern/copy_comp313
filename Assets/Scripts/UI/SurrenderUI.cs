using Mechanics;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SurrenderUI : MonoBehaviour
{
    [SerializeField] private GameObject SurrenderPanel;
    [SerializeField] private List<HealthComponent> towerHealComponents;
    private string mainMenuSceneNam = "main_menu";

    // Start is called before the first frame update
    void Start()
    {
       
            SurrenderPanel.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {

        // Check if the Escape key was pressed
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SurrenderPanel.SetActive(true);
        }
    }
    public void Surrender()
    {
        foreach (var component in towerHealComponents)
        {
            if (component.IsOwner) 
            {
                component.ReduceHealthServerRpc(999.9f);
                SurrenderPanel.SetActive(false);
                return;
          
            }
        }
    }
    public void NoSurrender()
    {
        SurrenderPanel.SetActive(false);
    }
}

    
   

//    [ServerRpc(RequireOwnership = false)]
//    private void ShowSurrenderPanelServerRpc()
//    {
//        ShowSurrenderPanelClientRpc();
//    }

//    [ClientRpc]
//    private void ShowSurrenderPanelClientRpc()
//    {
//        if (IsClient)
//        {
//            SurrenderPanel.SetActive(true);
//            Time.timeScale = 0;

//        }
//    }
//}
