
using Unity.Netcode;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "UnitStatus", menuName = "Unit", order = 0)]
public class ScriptableObjectUnits : ScriptableObject
{
    public uint objectInternalId;
    public string unitName;
    public Sprite smallSprite;
    public Sprite largeSprite;
    public int energyCost;
    public GameObject rig;
    public float healt;
    public float damage;
    public float shield;
    
}
