using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Unity.VisualScripting;
using Mechanics;

[RequireComponent(typeof(Button))]
public class UnitButtonTrigger : MonoBehaviour
{

   
    private Button _button;

    [SerializeField] private Image buttonImage;
    
    private ScriptableObjectUnits _reveivedData;

    public UnityEvent<ScriptableObjectUnits> OnButtonClick;
    

    //[SerializeField] private ControllerCanvasComponent canvasComponent;
    // Start is called before the first frame update
    void Start()
    {
        _button = GetComponent<Button>();
        Debug.Assert(_button != null);
        _button.onClick.AddListener(HandleButtonClick);

    }

    public void SetData(ScriptableObjectUnits newData)
    {
        _reveivedData = newData;
        buttonImage.sprite = _reveivedData.smallSprite;
    }


    private void HandleButtonClick()
    {
        Debug.Log("Button Clicked");
        OnButtonClick.Invoke(_reveivedData);

    }

    private void OnDestroy()
    {
        _button.onClick.RemoveListener(HandleButtonClick);
    }


}
