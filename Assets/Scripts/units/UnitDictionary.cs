﻿using System.Collections.Generic;
using UnityEngine;

namespace units
{
    [CreateAssetMenu(fileName = "UnitDictionary", menuName = "Unit Dictionary", order = 0)]
    public class UnitDictionary : ScriptableObject
    {
        [SerializeField] private List<ScriptableObjectUnits> availableUnits = new List<ScriptableObjectUnits>();

        public ScriptableObjectUnits GetUnitById(uint unitId)
        {
            foreach (var unit in availableUnits)
            {
                if (unitId == unit.objectInternalId)
                {
                    return unit;
                }
            }
            return null;
        }

        public List<ScriptableObjectUnits> GetUnitsByIds(List<uint> unitIdList)
        {
            var unitsFound = new List<ScriptableObjectUnits>();
            
            foreach (var unit in availableUnits)
            {
                var index = unitIdList.IndexOf(unit.objectInternalId);
                if (index != -1)
                {
                    unitsFound.Add(unit);
                }
            }

            return unitsFound;
        }
    }
}