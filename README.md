Create Script Spawner:

![image](https://github.com/SebastianSilvaDev/COMP313002_Group1/assets/60013030/445ec475-6dab-44d2-bb4a-7ab2fdcd84ed)

Create Button for CardItem:

![image](https://github.com/SebastianSilvaDev/COMP313002_Group1/assets/60013030/66c3d089-1d3a-4000-bde6-462d9cd85e6f)

Create 2 Spawn Point and add UI/Image for CardItem:

![image](https://github.com/SebastianSilvaDev/COMP313002_Group1/assets/60013030/b563a09f-0a98-4a7f-8c24-3ac1781135c4)

Create Icon Prefabs:

![image](https://github.com/SebastianSilvaDev/COMP313002_Group1/assets/60013030/d0b163fe-5db1-406a-b01c-dd5e7a3f5960)

Put Icon to show up what button will Spawn:

![image](https://github.com/SebastianSilvaDev/COMP313002_Group1/assets/60013030/62750c94-9a07-4cd9-80e9-671b56db7e69)

